﻿using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class GameplayUIScreen : UIScreen
{
    public TMP_Text tx;
    [SerializeField] PlayerInput _currentMap;
    [SerializeField] GameObject _smallPanel;

    [SerializeField] float _timer = 1.2f;
    float _lastTime;

    protected override void OnEnable()
    {
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
        Time.timeScale = 1f; // resume the game time
        Cursor.lockState = CursorLockMode.Locked;
        _currentMap.SwitchCurrentActionMap("GameMode");
    }

    void Update()
    {
        if (_lastTime + _timer < Time.time)
        {
            _smallPanel.SetActive(false);
        }
    }

    public void CoffeInteraction()
    {
        _smallPanel.SetActive(true);
        _lastTime = Time.time;
    }
}
