﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseUIScreen : UIScreen, IDataReceiver<EnemyData>
{

    protected override void OnEnable()
    {
        base.OnEnable();
        Time.timeScale = 0f; //Pause the game time
        Cursor.lockState = CursorLockMode.None;
    }

    public void ButtonContinue()
    {
        UIScreenController.ShowScreen<GameplayUIScreen>();
    }

    public void ButtonQuit()
    {
        UIScreenController.ShowScreen<MainMenuUIScreen>();
        SceneManager.LoadScene("MainMenuScene");
    }

    public void SetData(EnemyData data)
    {

    }
}
