
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;
public class CombatUIScreen : UIScreen, IDataReceiver<EnemyData>
{
    [SerializeField] PlayerInput _currentMap;
    [SerializeField] GameEvent _onPlayerAttack;
    [SerializeField] GameEvent _onEnemyAttack;
    [SerializeField] GameEvent _onCombatEnd;
    [SerializeField] GameEvent _onVictory;
    [SerializeField] GameEvent _onDefeat;
    [SerializeField] PlayerData _player;
    // [SerializeField] TMP_Text _txt;

    private EnemyData _enemyData;


    protected override void OnEnable()
    {
        base.OnEnable();
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 1f;
        _currentMap.SwitchCurrentActionMap("UI");
    }

    public void SetData(EnemyData item)
    {
        _enemyData = item;
    }

    public void ButtonAttack()
    {
        // _txt.text = PlayerData.Actions[0];
        _onPlayerAttack.Invoke();
        _enemyData.Health -= _player.Damage;
        EnemyCheck();
    }

    public void ButtonSpecialAttack()
    {
        // _txt.text = PlayerData.Actions[1];
        _onPlayerAttack.Invoke();
        _enemyData.Health -= _player.Damage * 2f;
        StartCoroutine(EnemyTurn());
    }

    public void ButtonHeal()
    {
        if (_player.CoffeAmount > 0)
        {
            // _txt.text = PlayerData.Actions[2];
            _player.DrinkCoffe();
            StartCoroutine(EnemyTurn());
        }
        // else _txt.text = "Out of Coffe";
    }

    private void EnemyCheck()
    {
        if (_enemyData.Health <= 0)
        {
            _onCombatEnd.Invoke();
            _onVictory.Invoke();
            UIScreenController.ShowScreen<GameplayUIScreen>();
        }
        else StartCoroutine(EnemyTurn());
    }

    private IEnumerator EnemyTurn()
    {
        EventSystem.current.sendNavigationEvents = false;
        yield return new WaitForSeconds(0.75f);
        _onEnemyAttack.Invoke();
        // _txt.text = $"{_enemyData.Name} attacks you.";
        _player.Life -= _enemyData.Damage;
        Debug.Log($"PlayerHealth: {_player.Life} - Enemy Health: {_enemyData.Health}");
        yield return new WaitForSeconds(0.5f);
        if (_player.Life <= 0)
        {
            EventSystem.current.sendNavigationEvents = true;
            _onCombatEnd.Invoke();
            _onDefeat.Invoke();
            UIScreenController.ShowScreen<EndUIScreen>();
        }
        else
        {
            EventSystem.current.sendNavigationEvents = true;
            base.OnEnable();
        }
    }
}