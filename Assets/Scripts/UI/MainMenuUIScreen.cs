﻿using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuUIScreen : UIScreen
{
    [SerializeField] TMP_Text _tx;

    [SerializeField] PlayerInput _currentMap;
    [SerializeField] PlayerData _playeData;

    protected override void OnEnable()
    {
        base.OnEnable();
        _playeData.Reset();
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 1f;
        _currentMap.SwitchCurrentActionMap("UI");
    }

    public void ButtonPlay()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
        UIScreenController.ShowScreen<GameplayUIScreen>();
    }

    public void ButtonControls()
    {
        UIScreenController.ShowScreen<ControlsUIScreen>();
    }

    public void ButtonQuit()
    {
        Application.Quit();
    }
}
