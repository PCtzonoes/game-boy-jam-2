﻿using UnityEngine;
using UnityEngine.EventSystems;

public abstract class UIScreen : MonoBehaviour
{
    public UnityEngine.UI.Button FirstButton;

    protected virtual void OnEnable()
    {
        // EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(FirstButton.gameObject);
        EventSystem.current.UpdateModules();
        // _firstButton.Select();

    }
}
