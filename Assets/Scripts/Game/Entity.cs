using UnityEngine;

public class Entity : MonoBehaviour
{
    [SerializeField] Animator _anim;
    [SerializeField] SpriteRenderer _spriteRend;
    public void Initialize(AnimatorOverrideController anim, Sprite sprite)
    {
        _spriteRend.sprite = sprite;
        _anim.runtimeAnimatorController = anim;
        _spriteRend.enabled = true;
        _anim.enabled = true;
    }

    public void Attack()
    {
        _anim.SetTrigger("Attacking");
    }

    public void Hurt()
    {
        _anim.SetTrigger("IsAttacked");
    }
}
