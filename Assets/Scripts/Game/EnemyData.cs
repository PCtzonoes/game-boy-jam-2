using System.Collections.Generic;

[System.Serializable]
public struct EnemyData
{
    public string Name;
    public float Health;
    public float Damage;
    public List<string> Speeches;
}