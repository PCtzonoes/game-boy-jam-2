using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFix : MonoBehaviour
{
    void OnEnable()
    {
        var temp = FindObjectOfType<Canvas>();
        temp.worldCamera = GetComponent<Camera>();
    }
}
