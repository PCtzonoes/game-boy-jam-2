using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] Sprite _enemySprite;
    [SerializeField] AnimatorOverrideController _anim;
    [SerializeField] EnemyData stats;
    [SerializeField] GameEvent _onCombatStart;
    [SerializeField] Entity _mapEntity;
    private void OnTriggerEnter2D(Collider2D other)
    {
        UIScreenController.ShowScreen<CombatUIScreen>();
        _mapEntity.Initialize(_anim, _enemySprite);
        UIScreenController.AddData<CombatUIScreen, EnemyData>(stats);
        _onCombatStart.Invoke();
        gameObject.SetActive(false);
    }
}
