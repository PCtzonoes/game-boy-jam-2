using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    [SerializeField]
    protected GameEvent _gameEvent;

    [SerializeField]
    protected UnityEvent _unityEvent;

    private void OnEnable()
    {
        OnEnableGameEvent();
    }

    private void OnDisable()
    {
        OnDisableGameEvent();
    }

    virtual
    protected void OnEnableGameEvent()
    {
        _gameEvent.Register(this);
    }

    virtual
    protected void OnDisableGameEvent()
    {
        _gameEvent.Remove(this);
    }

    virtual
    public void RaiseEvent()
    {
        _unityEvent.Invoke();
    }
}
