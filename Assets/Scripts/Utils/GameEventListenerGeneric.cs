using UnityEngine;
using UnityEngine.Events;

public abstract class GameEventListenerGeneric<T> : MonoBehaviour
{
    [SerializeField]
    protected GameEventGeneric<T> _gameEvent;

    [SerializeField]
    protected UnityEvent _unityEvent;

    private void OnEnable()
    {
        OnEnableGameEvent();
    }

    private void OnDisable()
    {
        OnDisableGameEvent();
    }

    virtual
    protected void OnEnableGameEvent()
    {
        _gameEvent.Register(this);
    }

    virtual
    protected void OnDisableGameEvent()
    {
        _gameEvent.Remove(this);
    }

    virtual
    public void RaiseEvent()
    {
        _unityEvent.Invoke();
    }
}
