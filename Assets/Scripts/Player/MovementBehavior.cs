using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class MovementBehavior : MonoBehaviour
{
    public float speed = 1;
    public float acceleration = 2;
    public Vector2 nextMoveCommand;
    public Animator animator;
    public bool flipX = false;

    new Rigidbody2D rigidbody2D;
    SpriteRenderer spriteRenderer;
    PixelPerfectCamera pixelPerfectCamera;

    Vector2 start, end;
    Vector2 currentVelocity;
    float startTime;
    float distance;
    float velocity;

    void Move()
    {
        if (nextMoveCommand == Vector2.zero)
        {
            velocity = 0;
            rigidbody2D.velocity = Vector2.zero;
        }
        else
        {
            velocity += acceleration;
            UpdateAnimator(nextMoveCommand);
        }
        rigidbody2D.velocity += nextMoveCommand.normalized * velocity * Time.deltaTime;
        rigidbody2D.velocity = Vector2.ClampMagnitude(rigidbody2D.velocity, speed);
    }

    void UpdateAnimator(Vector2 direction)
    {
        if (animator)
        {
            animator.SetInteger("WalkX", direction.x < 0 ? -1 : direction.x > 0 ? 1 : 0);
            animator.SetInteger("WalkY", direction.y < 0 ? -1 : direction.y > 0 ? 1 : 0);
        }
    }

    void Update()
    {
        Move();
    }

    void LateUpdate()
    {
        if (pixelPerfectCamera != null)
        {
            transform.position = pixelPerfectCamera.RoundToPixel(transform.position);
        }
    }

    void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        pixelPerfectCamera = GameObject.FindObjectOfType<PixelPerfectCamera>();
    }
}

