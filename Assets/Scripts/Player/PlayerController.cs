using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Interactions;

public class PlayerController : MonoBehaviour
{
    [SerializeField] MovementBehavior _playerMovement;
    [SerializeField] InputActionReference _axis;
    [SerializeField] InputActionReference _buttonA;
    [SerializeField] InputActionReference _buttonB;
    [SerializeField] InputActionReference _start;
    [SerializeField] InputActionReference _select;
    private Vector2 _rawAxis = new Vector2();

    private void OnEnable()
    {
        _axis.ToInputAction().performed += TheAxis;
        _buttonA.ToInputAction().performed += TheButtonA;
        _buttonB.ToInputAction().performed += TheButtonB;
        _start.ToInputAction().performed += OnTogglePause;
        _select.ToInputAction().performed += OnSelectToggle;
        _buttonA.ToInputAction().Enable();
        _start.ToInputAction().Enable();
        _buttonB.ToInputAction().Enable();
        _axis.ToInputAction().Enable();
        _select.ToInputAction().Enable();
        _playerMovement.nextMoveCommand = Vector2.zero;
    }

    void OnDisable()
    {
        _buttonA.ToInputAction().Disable();
        _start.ToInputAction().Disable();
        _buttonB.ToInputAction().Disable();
        _axis.ToInputAction().Disable();
        _select.ToInputAction().Disable();
        _axis.ToInputAction().performed -= TheAxis;
        _buttonA.ToInputAction().performed -= TheButtonA;
        _buttonB.ToInputAction().performed -= TheButtonB;
        _start.ToInputAction().performed -= OnTogglePause;
        _select.ToInputAction().performed -= OnSelectToggle;
        _playerMovement.nextMoveCommand = Vector2.zero;
    }

    public void TheAxis(InputAction.CallbackContext value)
    {
        _rawAxis = Vector2.zero;

        var axis = value.ReadValue<Vector2>();
        if (axis.x != 0)
            _rawAxis = new Vector2(axis.x, 0);
        else if (axis.y != 0) _rawAxis = new Vector2(0, axis.y);

        else _rawAxis = Vector2.zero;
    }

    public void TheButtonA(InputAction.CallbackContext value)
    {

    }

    public void TheButtonB(InputAction.CallbackContext value)
    {

    }

    public void OnTogglePause(InputAction.CallbackContext value)
    {

    }

    public void OnSelectToggle(InputAction.CallbackContext value)
    {

    }

    void Update()
    {
        _playerMovement.nextMoveCommand = _rawAxis;
    }
}
